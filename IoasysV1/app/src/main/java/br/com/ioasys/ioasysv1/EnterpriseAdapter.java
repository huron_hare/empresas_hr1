package br.com.ioasys.ioasysv1;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class EnterpriseAdapter extends RecyclerView.Adapter<EnterpriseAdapter.MyViewHolder> {

    private List<Enterprise> enterprise;

    public EnterpriseAdapter(List<Enterprise> Enterprises) {
        this.enterprise = Enterprises;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rtl_enterprise, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Enterprise ent = enterprise.get(position);
        holder.name.setText(ent.getEnterpriseName());
        holder.type.setText(ent.getEnterpriseType().getEnterpriseTypeName());
        holder.country.setText(ent.getCountry());
    }

    @Override
    public int getItemCount() {
        return enterprise.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, type, country;
        public ImageView back;

        public MyViewHolder(View view) {
            super(view);
            back = view.findViewById(R.id.img_empresa);
            name = view.findViewById(R.id.txt_nome_empresa);
            type = view.findViewById(R.id.txt_empresa_neg);
            country = view.findViewById(R.id.txt_empresa_pais);
        }

        public ImageView getBack() {
            return back;
        }
    }
}


