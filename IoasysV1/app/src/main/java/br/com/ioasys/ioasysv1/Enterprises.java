package br.com.ioasys.ioasysv1;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

;

interface EntClient {
    @Headers("Content-Type: application/json")
    @GET("/api/v1/enterprises/")
    Call<Enterprises> showEnt(
            @Header("access-token") String aToken,
            @Header("client") String client,
            @Header("uid") String uid);

}

interface EntClientF {
    @Headers("Content-Type: application/json")
    @GET("/api/v1/enterprises")
    Call<Enterprises> showEnt(
            @Query("name") String name,
            @Header("access-token") String aToken,
            @Header("client") String client,
            @Header("uid") String uid);

}

public class Enterprises {

    @SerializedName("enterprises")
    @Expose
    private List<Enterprise> enterprises = null;
    private String uid, client, access_token;

    public Enterprises(String uid, String client, String access_token) {
        this.uid = uid;
        this.client = client;
        this.access_token = access_token;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }

}
