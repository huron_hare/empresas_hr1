package br.com.ioasys.ioasysv1;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

interface UsrClient {
    @POST("/api/v1/users/auth/sign_in")
    Call<User> createUser(@Body User user);
}

class Portfolio {

    @SerializedName("enterprises_number")
    @Expose
    private Integer enterprisesNumber;
    @SerializedName("enterprises")
    @Expose
    private List<Object> enterprises = null;

    public Integer getEnterprisesNumber() {
        return enterprisesNumber;
    }

    public void setEnterprisesNumber(Integer enterprisesNumber) {
        this.enterprisesNumber = enterprisesNumber;
    }

    public List<Object> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Object> enterprises) {
        this.enterprises = enterprises;
    }

}

public class User {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("errors")
    @Expose
    private String errors;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

