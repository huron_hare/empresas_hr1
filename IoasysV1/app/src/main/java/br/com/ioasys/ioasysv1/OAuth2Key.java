package br.com.ioasys.ioasysv1;

import java.io.Serializable;

@SuppressWarnings("serial")
public class OAuth2Key implements Serializable {
    private String uid, client, access_token;

    public OAuth2Key(String uid, String client, String access_token) {
        this.uid = uid;
        this.client = client;
        this.access_token = access_token;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
