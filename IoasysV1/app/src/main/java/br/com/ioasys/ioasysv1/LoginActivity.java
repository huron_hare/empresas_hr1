package br.com.ioasys.ioasysv1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Connection con = new Connection();
        setContentView(R.layout.activity_login);
        Button button = findViewById(R.id.btn_login);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText login = findViewById(R.id.txt_Pesquisa);
                EditText psw = findViewById(R.id.txt_Psw);
                User usr = new User(
                        login.getText().toString(),
                        psw.getText().toString()
                );
                con.sendPostRequest(LoginActivity.this, usr);

            }
        });

    }

}
