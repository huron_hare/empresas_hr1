package br.com.ioasys.ioasysv1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

import java.util.ArrayList;
import java.util.List;

@GlideModule
public class ActivityController extends AppGlideModule {
    private List<Enterprise> ent = new ArrayList<>();
    private RecyclerView recyclerView;
    private EnterpriseAdapter eAdapter;

    public void SwitchAct(final OAuth2Key auth2Key, final Context context, final Context DesContext) {
        Intent myIntent = new Intent(context, DesContext.getClass());
        myIntent.putExtra("oAuth2", auth2Key);
        context.startActivity(myIntent);
    }

    public void SwitchAct(final Context context, final Context DesContext, Enterprise ent) {
        Intent myIntent = new Intent(context, DesContext.getClass());
        Bundle b = new Bundle();
        b.putString("name", ent.getEnterpriseName());
        b.putString("country", ent.getCountry());
        b.putString("type", ent.getEnterpriseType().getEnterpriseTypeName());
        b.putString("desc", ent.getDescription());
        myIntent.putExtras(b);
        context.startActivity(myIntent);

    }

    public void CreateEntModel(final Enterprises enterprises, final Activity atv, boolean lyt, final OAuth2Key auth2Key, final PesquisaActivity activity) {
        if (!lyt) {
            atv.setContentView(R.layout.activity_home);
        } else {
            atv.setContentView(R.layout.activity_pesquisa);
        }
        recyclerView = (RecyclerView) atv.findViewById(R.id.recycler_view);
        eAdapter = new EnterpriseAdapter(ent);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(atv);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(eAdapter);

        for (int i = 0; i < enterprises.getEnterprises().size(); i++) {
            try {
                Enterprise enter = new Enterprise(enterprises.getEnterprises().get(i).getEnterpriseName(), enterprises.getEnterprises().get(i).getCountry(), enterprises.getEnterprises().get(i).getEnterpriseType());

                //Failed attempt to load images with Glide
                //ImageView img = atv.findViewById(R.id.img_empresa);
                //Glide.with(atv).load("http://empresas.ioasys.com.br" + enterprises.getEnterprises().get(i).getPhoto().toString()).into(img);
                //enter.setPhoto(img);

                ent.add(enter);
                eAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                Log.d("Exception : ", e.getMessage());
            }
        }
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(atv, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Enterprise enterprise = enterprises.getEnterprises().get(position);
                InfoEntActivity inf = new InfoEntActivity();
                SwitchAct(atv, inf, enterprise);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        final Connection con = new Connection();
        final Enterprises ent = new Enterprises(auth2Key.getUid(), auth2Key.getClient(), auth2Key.getAccess_token());
        final TextView txt = atv.findViewById(R.id.txt_Pesquisa);
        final Button btn_close = atv.findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // não ficou claro pra mim nessa parte se era pra fechar a activity ou limpar o texto, devido ao nome do botão eu deixei pra fechar a activity mesmo.
                atv.finish();
            }
        });

        txt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int id, KeyEvent event) {
                if (id == EditorInfo.IME_ACTION_NEXT) {
                    con.sendPostRequest(ent, activity, v.getText().toString(), auth2Key, activity);
                    return true;
                }
                return false;
            }
        });
    }


}

