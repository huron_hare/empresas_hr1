package br.com.ioasys.ioasysv1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class HomeActivity extends AppCompatActivity {
    public void searchClick(View v) {

        ActivityController atv = new ActivityController();
        PesquisaActivity pesquisaActivity = new PesquisaActivity();
        Intent i = getIntent();
        final OAuth2Key key = (OAuth2Key) i.getSerializableExtra("oAuth2");
        atv.SwitchAct(key, this, pesquisaActivity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Intent i = getIntent();
        final OAuth2Key key = (OAuth2Key) i.getSerializableExtra("oAuth2");
        Connection con = new Connection();
        final Enterprises ent = new Enterprises(key.getUid(), key.getClient(), key.getAccess_token());
        PesquisaActivity activity = new PesquisaActivity();
        con.sendPostRequest(ent, this, key, activity);

    }

    public void startListeners() {


    }
}
