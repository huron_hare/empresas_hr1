package br.com.ioasys.ioasysv1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.Serializable;

public class InfoEntActivity extends AppCompatActivity implements Serializable {

    private TextView txtName, txtDesc, txtCountry, txtType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_ent);
        txtName = findViewById(R.id.txt_entinfo_name);
        txtDesc = findViewById(R.id.txt_entinfo_desc);
        txtCountry = findViewById(R.id.txt_empresa_pais);
        txtType = findViewById(R.id.txt_empresa_neg);
        Bundle b = getIntent().getExtras();
        Button button = (Button) findViewById(R.id.btn_back);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("LOGGING 1", "LOGGED");
                finish();
            }
        });
        createInfoPage(b.getString("name"), b.getString("country"), b.getString("type"), b.getString("desc"));
    }

    void createInfoPage(String name, String country, String type, String desc) {
        try {
            Log.d("Creation : ", name + country + type + desc);
            txtName.setText(name);
            txtDesc.setText(desc);
            txtCountry.setText(country);
            txtType.setText(type);
            //txtName.setText(enterprise.getEnterpriseName());
        } catch (Exception e) {
            Log.d("Exception : ", e.getMessage());
        }

    }
}
