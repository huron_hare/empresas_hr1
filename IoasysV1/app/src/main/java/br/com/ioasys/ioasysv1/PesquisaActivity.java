package br.com.ioasys.ioasysv1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

public class PesquisaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesquisa);

        Intent i = getIntent();
        OAuth2Key key = (OAuth2Key) i.getSerializableExtra("oAuth2");
        startListeners(key);

    }

    public void startListeners(final OAuth2Key key) {
        final Connection con = new Connection();
        final Enterprises ent = new Enterprises(key.getUid(), key.getClient(), key.getAccess_token());
        final TextView txt = findViewById(R.id.txt_Pesquisa);
        final Button btn_close = findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PesquisaActivity.this.finish();
            }
        });
        final PesquisaActivity activity = this;
        txt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int id, KeyEvent event) {
                if (id == EditorInfo.IME_ACTION_SEARCH) {
                    con.sendPostRequest(ent, PesquisaActivity.this, v.getText().toString(), key, activity);
                    return true;
                }
                return false;
            }
        });
    }

}
