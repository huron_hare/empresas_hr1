package br.com.ioasys.ioasysv1;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Connection {


    public void sendPostRequest(final Context context, User usr) {
        final boolean[] returned = new boolean[1];
        Retrofit builder = new Retrofit.Builder()
                .baseUrl("http://empresas.ioasys.com.br/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        UsrClient client = builder.create(UsrClient.class);
        Call<User> call = client.createUser(usr);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                try {
                    Log.d("CREATION", "Success : " + response.body().getSuccess() + "\n " + response.body().toString());

                    if (response.body().getSuccess()) {

                        Log.d("CREATION", "RESPOSTA : " + response.headers().toString() + "\n " + response.body().toString());
                        Toast.makeText(context, "succefull " + response.body(), Toast.LENGTH_SHORT).show();
                        ActivityController atv = new ActivityController();

                        OAuth2Key oAuth2Key = new OAuth2Key(
                                response.headers().values("uid").toString().replaceAll("([\\[\\]])", ""),
                                response.headers().values("client").toString().replaceAll("([\\[\\]])", ""),
                                response.headers().values("access-token").toString().replaceAll("([\\[\\]])", "")
                        );
                        HomeActivity act = new HomeActivity();
                        atv.SwitchAct(oAuth2Key, context, act);
                    }
                } catch (Exception ex) {
                    Log.d("CREATION", "Exception : " + ex + "\n ");
                    Toast.makeText(context, "Username or password incorrect " /*+ ex*/, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(context, "Fatal error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void sendPostRequest(final Enterprises ent, final Activity context, final OAuth2Key key, final PesquisaActivity activity) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        Retrofit builder = new Retrofit.Builder()
                .baseUrl("http://empresas.ioasys.com.br/")
                .addConverterFactory(GsonConverterFactory.create()).client(httpClient.build()).build();
        EntClient client = builder.create(EntClient.class);

        Call<Enterprises> call = client.showEnt(ent.getAccess_token(), ent.getClient(), ent.getUid());
        Log.d("CREATION", "Request : " + call.toString() + "\n ");

        call.enqueue(new Callback<Enterprises>() {
            @Override
            public void onResponse(Call<Enterprises> call, Response<Enterprises> response) {
                try {
                    if (response.body().getEnterprises().size() != 0) {
                        ActivityController activityController = new ActivityController();
                        activityController.CreateEntModel(response.body(), context, false, key, activity);
                        Log.d("CREATION", "Returned : " + "\n ");
                    }
                } catch (Exception ex) {
                    Log.d("CREATION", "Exception : " + ex + "\n ");
                }
            }

            @Override
            public void onFailure(Call<Enterprises> call, Throwable t) {
                Log.d("CREATION", "Error : " + t.getMessage() + "\n ");
            }
        });

    }

    public void sendPostRequest(final Enterprises ent, final PesquisaActivity pesquisaActivity, String name, final OAuth2Key key, final PesquisaActivity activity) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // add your other interceptors …

        // add logging as last interceptor
        httpClient.addInterceptor(logging);
        Retrofit builder = new Retrofit.Builder()
                .baseUrl("http://empresas.ioasys.com.br/")
                .addConverterFactory(GsonConverterFactory.create()).client(httpClient.build()).build();
        EntClientF client = builder.create(EntClientF.class);

        Call<Enterprises> call = client.showEnt(name, ent.getAccess_token(), ent.getClient(), ent.getUid());
        Log.d("CREATION", "Request : " + call.toString() + "\n ");

        call.enqueue(new Callback<Enterprises>() {
            @Override
            public void onResponse(Call<Enterprises> call, Response<Enterprises> response) {
                try {
                    if (response.body().getEnterprises().size() != 0) {
                        ActivityController atv = new ActivityController();
                        atv.CreateEntModel(response.body(), pesquisaActivity, true, key, activity);
                        Log.d("CREATION", "Pesquisa : " + response.body().getEnterprises().get(0).getEnterpriseName() + "\n ");
                    }
                } catch (Exception ex) {
                    Log.d("CREATION", "Exception : " + ex + "\n ");
                }
            }

            @Override
            public void onFailure(Call<Enterprises> call, Throwable t) {
                Log.d("CREATION", "Error : " + t.getMessage() + "\n ");
            }
        });

    }

}
